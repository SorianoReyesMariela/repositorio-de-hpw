function Alumno(nc,n,ap,am,g){
  
  
  var _tipo="alumno";
  var nControl=nc;
  var nombre=n;
  var aPat=ap;
  var aMat=am;
  var genero=g;
  
  function getControl(){
    return nControl;
  }
  
  function getNombre(){
    return nombre;
  }
  
  function getPaterno(){
    return aPat;
  }
  
  function getMaterno(){
    return aMat;
  }
  
  function getGenero(){
    return genero;
  }
  
  function setControl(newControl){
    nControl=newControl;
  }
  
  function setNombre(newNombre){
    nombre=newNombre;
  }
  
  function setPaterno(paterno){
    aPat=paterno;
  }
  
  function setMaterno(materno){
    aMat=materno;
  }
  
  function setGenero(newGen){
    genero=newGen;
  }
  return{
    "tipo":       _tipo,
    "getControl": getControl,
    "getNombre":  getNombre,
    "getPaterno": getPaterno,
    "getMaterno": getMaterno,
    "getGenero":  getGenero,
    "setControl": setControl,
    "setNombre":  setNombre,
    "setPaterno": setPaterno,
    "setMaterno": setMaterno,
    "setGenero":  setGenero
    
  };
}




