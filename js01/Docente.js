function Docente(i,r,n,ap,am,g){
  var _tipo="docente";
  var id=i;
  var rfc=r;
  var nombre=n;
  var apellido_paterno=ap;
  var apellido_materno=am;
  var genero=g;

  return{
    "tipo":_tipo,
    "id":id,
    "rfc":rfc,
    "nombre":nombre,
    "apellido_paterno":apellido_paterno,
    "apellido_materno":apellido_materno,
    "genero":genero,
  };
}
