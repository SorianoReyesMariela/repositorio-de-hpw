function Grupo(i,c,n){
  
  var id=i;
  var clave=c;
  var nombre=n;
  var alumnos=[30];
  var a=0;
  var docentes=[7];
  var d=0;
  var materias=[7];
  var m=0;

  function que_es(objeto){
    return objeto.tipo;
  } 

function alumno_existe(alumno){
  var es=que_es(alumno);
  if(es==="alumno"){
    for(var i=0;i<alumnos.length;i++){
      buscado=alumnos[i];
      if(buscado.numero_de_control===alumno.numero_de_comtrol){
        return true;
      }else{
        return false;
      }
      
    }
  }else{
    return true;
  }
  
}

function docente_existe(docente){
  var es=que_es(docente);
  if(es==="docente"){
    for(var i=0;i<docentes.length;i++){
      buscado=docentes[i];
      if(buscado.rfc===docente.rfc){
        return true;
      }else{
        return false;
      }
      
    }
  }else{
    return true;
  }
  
}

function materia_existe(materia){
  var es=que_es(materia);
  if(es==="materia"){
    for(var i=0;i<materias.length;i++){
      buscado=docentes[i];
      if(buscado.id===materia.id){
        return true;
      }else{
        return false;
      }
      
    }
  }else{
    return true;
  }
  
}

function _agregar_alumno(alumno){
  var existe=alumno_existe(alumno);
  if(existe===false && a<materias.length){
    alumnos.push(alumno);
    a++;
  }
}

function _agregar_docente(docente){
  var existe=docente_existe(docente);
  if(existe===false&& d<materias.length){
    docentes.push(docente);
    d++;
  }
}

function _agregar_materia(materia){
  var existe=materia_existe(materia);
  if(existe===false && m<materias.length){
    materias.push(materia);
    m++;
  }
}

 function listar_docentes(){
     for(var i=0; i<d;i++){
       var cont=docentes[i];
       console.log(cont.id+", "+cont.nombre+" "+cont.apellido_paterno+" "+cont.apellido_paterno+", "+ cont.genero);
     }
  }
  
  function listar_alumnos(){
     for(var i=0; i<a;i++){
       var cont=alumnos[i];
       console.log(cont.id+", "+cont.rfc+", "+cont.nombre+" "+cont.apellido_paterno+" "+cont.apellido_paterno+", "+ cont.genero);
     }
  }
  
  function listar_materias(){
     for(var i=0; i<m;i++){
       var cont=materias[i];
       console.log(cont.id+", "+cont.clave+", "+cont.nombre);
     }
  }
  

return{
  "id":id,
  "clave":clave,
  "nombre":nombre,
 "agregar_alumno":_agregar_alumno({"tipo":""}),
  "agregar_docente":_agregar_docente({"tipo":""}),
  "agregar_materia":_agregar_materia({"tipo":""}),
  "listar_alumnos":listar_alumnos(),
  "listar_docentes":listar_docentes(),
  "listar_materias":listar_materias()
};
}
