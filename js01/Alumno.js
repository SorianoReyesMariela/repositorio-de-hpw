function Alumno(i,nc,n,ap,am,g){
  _tipo="alumno";
  var id=i;
  var numero_de_control=nc;
  var nombre=n;
  var apellido_paterno=ap;
  var apellido_materno=am;
  var genero=g;
  var calificaciones=[7];

  return{
    "tipo":_tipo,
   "id":id,
   "numero_de_control":numero_de_control,
   "nombre":nombre,
    "apellido_paterno":apellido_paterno,
    "apellido_materno":apellido_materno,
    "genero":genero,
    "calificaciones":calificaciones
  };
}
